def power_of_two(number):
    value=False
    while number>1:
        if number%2!=0:
           value=True
           break
        else:
            number=number/2
    return value


num=input("Enter number to show if it is power of two:")
if power_of_two(int(num)):
     print ("number is not power of two")
else:
     print ("number is power of two")