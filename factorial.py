#bejme nje cikel nga nje deri tek vet numri (range(number+1)) pasi number duhet
# te perfshihet i shumzojme te gjithe ,rezultati ruhet ne variablin f ,ai kthehet ne fund

def iterative_factorial(number):
    f=1
    for x in range(1,number+1):
       f=f*x
    return f

number=input("Enter a number:")
print ("Factorial of number is "+ str(iterative_factorial(int(number))))